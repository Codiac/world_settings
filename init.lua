local S = minetest.get_translator(minetest.get_current_modname())

world_settings = {}

local settings = Settings(minetest.get_worldpath() .. DIR_DELIM .. "world_settings.conf")
local mod_flow = minetest.get_modpath("mcl_flow")

local world_settings_meta = {}
local domains = {}

local function full_name(domain, name)
	if string.find(domain, ".", 1, true) or string.find(name, ".", 1, true) then
		minetest.log("error", S("Setting domains and names cannot contain a dot"))
		return
	end
	if string.match(domain, "%s") or string.match(name, "%s") then
		minetest.log("error", S("Setting domains and names cannot contain whitespace"))
		return
	end

	return domain .. "." .. name
end

local full_name_regex = "^([^.]+)%.([^.]+)$"

function world_settings.split_full_name(full_name)
	return string.match(full_name, full_name_regex)
end

local function incriment_domain(domain)
	if domains[domain] == nil then
		domains[domain] = 0
	end
	domains[domain] = domains[domain] + 1
end

local function decriment_domain(domain)
	if domains[domain] ~= nil then
		domains[domain] = domains[domain] - 1
		if domains[domain] <= 0 then
			domains[domain] = nil
		end
	end
end

function world_settings.register_string(domain, name, default, help)
	local full_name = full_name(domain, name)
	if not full_name then
		return
	end

	incriment_domain(domain)

	if not settings:has(full_name) then
		minetest.log("info", S("Registering setting @1 = @2", full_name, tostring(default)))
		settings:set(full_name, default)
		if not settings:write() then
			minetest.log("action", S("Setting @1 could not be saved!", full_name))
			return
		end
	end

	world_settings_meta[full_name] = { type = "string", default = default, help = help }

	return world_settings.get(domain, name)
end

function world_settings.register_number(domain, name, default, help)
	local full_name = full_name(domain, name)
	if not full_name then
		return
	end

	incriment_domain(domain)

	if not settings:has(full_name) then
		minetest.log("info", S("Registering setting @1 = @2", full_name, tostring(default)))
		settings:set(full_name, default)
		if not settings:write() then
			minetest.log("action", S("Setting @1 could not be saved!", full_name))
			return
		end
	end

	world_settings_meta[full_name] = { type = "number", default = default, help = help }

	return world_settings.get(domain, name)
end

function world_settings.register_bool(domain, name, default, help)
	local full_name = full_name(domain, name)
	if not full_name then
		return
	end

	incriment_domain(domain)

	if not settings:has(full_name) then
		minetest.log("info", S("Registering setting @1 = @2", full_name, tostring(default)))
		settings:set_bool(full_name, default)
		if not settings:write() then
			minetest.log("action", S("Setting @1 could not be saved!", full_name))
		end
	end

	world_settings_meta[full_name] = { type = "bool", default = default, help = help }

	return world_settings.get(domain, name)
end

function world_settings.unregister(domain, name)
	local full_name = full_name(domain, name)
	if not full_name then
		return
	end

	if not settings:has(full_name) then
		minetest.log("warning", S("Can't unregister the setting @1, it has not been registered", full_name))
		return
	end

	if settings:remove(full_name) then
		minetest.log("action", S("The setting @1 has been removed", full_name))
		decriment_domain(domain)
	else
		minetest.log("action", S("The setting @1 could not be removed", full_name))
	end
end

function world_settings.get(domain, name)
	local full_name = full_name(domain, name)
	if not full_name then
		return
	end

	if not settings:has(full_name) then
		return S("Can't get the setting @1, it has not been registered", full_name)
	end

	local type = world_settings_meta[full_name]["type"]

	if type == "bool" then
		return settings:get_bool(full_name)
	elseif type == "number" then
		return tonumber(settings:get(full_name))
	else
		return settings:get(full_name)
	end
end

function world_settings.set(domain, name, value)
	local full_name = full_name(domain, name)
	if not full_name then
		return
	end

	if not settings:has(full_name) then
		minetest.log("warning", S("Can't set the setting @1, it has not been registered", full_name))
		return
	end

	local type = world_settings_meta[full_name]["type"]

	if type == "bool" then
		if value == "false" or value == "0" then
			value = false
		end
		settings:set_bool(full_name, value)
	else
		settings:set(full_name, value)
	end

	if settings:write() then
		minetest.log("info", S("Setting @1 is set to @2", full_name, tostring(value)))
	else
		minetest.log(S("warning", "Setting @1 could not be saved!", full_name, tostring(value)))
	end
end

function world_settings.get_names()
	local names = {}

	for _, k in pairs(settings:get_names()) do
		if world_settings_meta[k] then
			table.insert(names, k)
		end
	end

	table.sort(names)

	return names
end

function world_settings.get_domains()
	local dnames = {}

	for k, v in pairs(domains) do
		table.insert(dnames, k)
	end

	table.sort(dnames)

	return dnames
end

----- Start UI -----

local gui

if mod_flow then
	gui = flow.widgets
end

local function widget_for_setting(domain, name)
	local widget

	local full_name = full_name(domain, name)
	local type = world_settings_meta[full_name]["type"]

	if type == "bool" then
		widget = gui.Checkbox({
			name = full_name,
			label = name,
			selected = world_settings.get(domain, name),
		})
	elseif type == "number" then
		widget = gui.Field({
			expand = true,
			name = full_name,
			label = name,
			default = tostring(world_settings.get(domain, name)),
		})
	else
		widget = gui.Field({
			expand = true,
			name = full_name,
			label = name,
			default = world_settings.get(domain, name),
		})
	end

	return widget
end

local function update_domain_settings(player, ctx)
	local domains = world_settings.get_domains()
	local domain = domains[ctx.form.domain or 1]

	for _, key in pairs(world_settings.get_names()) do
		local dom, sname = world_settings.split_full_name(key)
		if dom == domain then
			if ctx.form[key] then
				world_settings.set(domain, sname, ctx.form[key])
			end
		end
	end
end

local my_gui

if mod_flow then
	my_gui = flow.make_gui(function(player, ctx)
		local domains = world_settings.get_domains()

		local domain = domains[ctx.form.domain or 1]
		local domain_settings = {}

		for _, key in pairs(world_settings.get_names()) do
			local dom, sname = world_settings.split_full_name(key)
			if dom == domain then
				table.insert(domain_settings, widget_for_setting(dom, sname))
			end
		end

		return gui.VBox({
			--min_w = 8,
			--min_h = 9,
			gui.HBox({
				gui.Label({ label = S("Domain") }),
				gui.Dropdown({
					name = "domain",
					items = domains,
					index_event = true,
				}),
			}),
			gui.Spacer({}),
			gui.VBox(domain_settings),
			gui.Spacer({}),
			gui.HBox({
				gui.Button({
					name = "save_button",
					label = S("Save"),
					on_event = function(player, ctx)
						update_domain_settings(player, ctx)
					end,
				}),
			}),
		})
	end)
end

----- End UI -----

minetest.register_chatcommand("wset", {
	description = S("Display or change world settings"),
	params = S("[ help <domain> | ui | <name> | <name> <value> ]"),
	privs = { server = true },
	func = function(name, param)
		if param == "" then
			for _, key in pairs(world_settings.get_names()) do
				local domain, sname = world_settings.split_full_name(key)
				minetest.chat_send_player(name, string.format("%s: %s", key, world_settings.get(domain, sname)))
			end
		elseif param == "ui" then
			if mod_flow then
				my_gui:show(minetest.get_player_by_name(name))
			else
				minetest.chat_send_player(name, "The flow mod is not installed, no UI possible.")
			end
		elseif param == "help" then
			for _, key in pairs(world_settings.get_names()) do
				minetest.chat_send_player(name, string.format("%s: %s", key, world_settings_meta[key]["help"]))
			end
		else
			local sparam = param:split(" ")
			local key = sparam[1]
			local value = sparam[2]
			if key == "help" then
				for _, key in pairs(world_settings.get_names()) do
					local domain, sname = world_settings.split_full_name(key)
					if value == domain then
						minetest.chat_send_player(name, string.format("%s: %s", key, world_settings_meta[key]["help"]))
					end
				end
			elseif value ~= nil then
				local domain, sname = world_settings.split_full_name(key)
				if domain and sname then
					world_settings.set(domain, sname, value)
				else
					minetest.chat_send_player(name, S("The setting @1 could not be found", key))
				end
			else
				local domain, sname = world_settings.split_full_name(key)
				if domain and sname then
					minetest.chat_send_player(name, string.format("%s: %s", key, world_settings.get(domain, sname)))
				else
					minetest.chat_send_player(name, S("The setting @1 could not be found", key))
				end
			end
		end
	end,
})
