# World Settings

This mod adds an API for managing per-world settings for Minetest games and mods.

Settings can be managed in code using the API and in game by players with the
`server` privilege using the `wset` chat command.

Settings are saved to the `world_settings.conf` file in the world's directory.

Settings are updated in the config file immediately on being registered or
altered.

Setting names consist of two parts, a domain and a name. The domain should be
unique for your game or mod. This allows settings in different mods to use the
same name.

Domain and name cannot contain a dot '.' os whitespace, if they do they will be rejected.

Once a setting has been set on the world, which will happen automatically
when they are registered, any changes to the default in the code, or in the
global settings, will not affect the world setting.

You can copy a world setting file to a new world after it is created but before
it is run to have a standard set of world settings applied to a new world.

You will need to restart a world if you want to change the settings by updating
the config file.

## Registering Settings

There is a function for each data type to register settings.

If the setting exists in the config for this world then the value is not
changed when calling the register functions.

"help" is the help text for the setting.

You usually do this in a global scope.

### world_settings.register_bool(domain, name, default, help)

### world_settings.register_number(domain, name, default, help)

### world_settings.register_string(domain, name, default, help)

## Un-Registering settings

If a setting is no longer required it can be removed.

### world_settings.unregister(domain, name)

## Getting settings

### world_settings.get(domain, name)

Gets the value for the setting.

Note that as world settings can be changed while the world is running they
should be gotten in the local scope before being used and not cached in a global
variable as is standard practice for global settings.

## Updating settings

### world_settings.set(domain, name, value)

## Utility functions

### world_settings.get_names()

Gets the full names for all settings.

### world_settings.split_full_name

Splits a full name in to domain and name.

## Chat command

Players with the `server` privilege can interact with the world settings via the
`wset` chat command.

* ```/wset``` with no arguments displays all current settings and their values.
* ```/wset help``` with no argument displays all current settings with their help text.
* ```/wset help <domain>``` with an argument displays settings for the specified domain with their help text.
* ```/wset name``` displays the value of the named setting.
* ```/wset name value``` changes the value of the named setting to the provided
value.

**NOTE** The `ui` option will show a very experimental form for changing
settings. Do not use it unless your world insurance covers extreme risk taking!

## Examples

### Register a number

Usually done in global scope.

```lua
world_settings.register_number(
	"my_mod",
	"max_volume",
	11,
	S("The maximum volume something can be played at")
)
```

### Get the number

Usually done in local scope.

```lua
local max_vol = world_settings.get("my_mod", "max_volume") 
```

### Register a boolean based on a global setting

```lua
world_settings.register_bool(
	"my_mod",
	"can_fu",
	minetest.settings:get_bool("my_mod_can_fu", true),
	S("Is this mod allowed to fu?")
)
```

### Query a setting in game

```shell
/wset my_mod.can_fu
```

### Change a setting in game

```shell
/wset my_mod.can_fu false
```

## TODO

???
